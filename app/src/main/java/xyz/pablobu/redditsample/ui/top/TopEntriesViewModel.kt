package xyz.pablobu.redditsample.ui.top

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import xyz.pablobu.redditsample.model.RedditEntry
import xyz.pablobu.redditsample.repository.TopEntriesRepository

class TopEntriesViewModel(private val repository: TopEntriesRepository) : ViewModel() {

    private val _entries = MutableLiveData<List<RedditEntry>>()
    val entries: LiveData<List<RedditEntry>>
        get() = _entries

    private val _selectedEntry = MutableLiveData<RedditEntry>()
    val selectedEntry: LiveData<RedditEntry>
        get() = _selectedEntry

    fun refreshEntries() {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                _entries.postValue(repository.refreshTopEntries())
            }
        }
    }

    fun onEntrySelected(entry: RedditEntry) {
        _selectedEntry.value = entry
        repository.markEntryAsRead(entry)
        // this could be simplified by exposing the entries from the repository in a flow and
        // updating it in masEntryAsRead
        _entries.value = repository.getTopEntries()
    }
}
