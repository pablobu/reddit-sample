package xyz.pablobu.redditsample.ui.top

import android.text.format.DateUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.view.isGone
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.facebook.drawee.view.SimpleDraweeView
import kotlinx.android.synthetic.main.row_entry.view.*
import xyz.pablobu.redditsample.R
import xyz.pablobu.redditsample.model.RedditEntry
import java.util.Date

class RedditEntryAdapter(val onClick: (entry: RedditEntry) -> Unit) :
    RecyclerView.Adapter<EntryVH>() {

    var entries: List<RedditEntry> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun getItemCount() = entries.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EntryVH {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_entry, parent, false)
        return EntryVH(view)
    }

    override fun onBindViewHolder(holder: EntryVH, position: Int) {
        val entry = entries[position]
        val context = holder.itemView.context
        holder.apply {
            title.text = entry.title
            author.text = entry.author
            comments.text = context.getString(R.string.number_of_comments, entry.numComments)

            published.text = DateUtils.getRelativeTimeSpanString(
                entry.created,
                Date().time,
                DateUtils.MINUTE_IN_MILLIS,
                DateUtils.FORMAT_ABBREV_ALL
            )

            unreadIcon.isVisible = entry.unread
            image.isGone = !entry.hasImage
            if (entry.hasImage) {
                image.setImageURI(entry.thumbnail)
            }

            itemView.setOnClickListener {
                onClick(entries[this.adapterPosition])
            }
        }
    }
}

class EntryVH(view: View) : RecyclerView.ViewHolder(view) {
    val title: TextView get() = itemView.title
    val author: TextView get() = itemView.author
    val comments: TextView get() = itemView.comments
    val published: TextView get() = itemView.published
    val image: SimpleDraweeView get() = itemView.image
    val unreadIcon: ImageView get() = itemView.unread
}