package xyz.pablobu.redditsample.ui.top

import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.text.format.DateUtils
import androidx.appcompat.app.AppCompatActivity
import androidx.core.net.toUri
import androidx.core.view.isGone
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_top_entries.topEntries
import kotlinx.android.synthetic.main.post_view.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import xyz.pablobu.redditsample.R
import xyz.pablobu.redditsample.model.RedditEntry
import xyz.pablobu.redditsample.ui.entry.EntryActivity
import java.util.Date

class TopEntriesActivity : AppCompatActivity() {

    private val viewModel: TopEntriesViewModel by viewModel()
    private val adapter = RedditEntryAdapter {
        viewModel.onEntrySelected(it)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bindViews()

        viewModel.entries.observe(this, Observer {
            adapter.entries = it
            swipeRefreshLayout.isRefreshing = false
        })
        viewModel.selectedEntry.observe(this, Observer {
            showEntry(it)
        })

        viewModel.refreshEntries()
    }

    private fun bindViews() {
        topEntries.layoutManager = LinearLayoutManager(this)
        topEntries.adapter = adapter

        swipeRefreshLayout.setOnRefreshListener {
            viewModel.refreshEntries()
        }
    }

    private fun showEntry(entry: RedditEntry) {
        if (resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT) {
            EntryActivity.start(this, entry)
        } else {
            entry_title.text = entry.title
            entry_author.text = entry.author
            entry_comments.text = getString(R.string.number_of_comments, entry.numComments)
            entry_published.text = DateUtils.getRelativeTimeSpanString(
                entry.created,
                Date().time,
                DateUtils.MINUTE_IN_MILLIS,
                DateUtils.FORMAT_ABBREV_ALL
            )
            entry_image.isGone = !entry.hasImage
            if (entry.hasImage) {
                entry_image.setImageURI(entry.thumbnail)
                entry_image.setOnClickListener {
                    startActivity(
                        Intent(Intent.ACTION_VIEW).apply { data = entry.thumbnail.toUri() }
                    )
                }
            }
        }
    }
}
