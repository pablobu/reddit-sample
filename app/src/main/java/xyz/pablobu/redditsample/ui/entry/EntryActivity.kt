package xyz.pablobu.redditsample.ui.entry

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.net.toUri
import androidx.core.view.isGone
import kotlinx.android.synthetic.main.post_view.*
import xyz.pablobu.redditsample.R
import xyz.pablobu.redditsample.model.RedditEntry

class EntryActivity : AppCompatActivity() {

    companion object {
        private const val EXT_ENTRY = "ext-entry"

        fun start(context: Context, entry: RedditEntry) {
            val intent = Intent(context, EntryActivity::class.java).apply {
                putExtra(EXT_ENTRY, entry)
            }
            context.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.post_view)

        val entry = intent.getParcelableExtra<RedditEntry>(EXT_ENTRY)
        entry_title.text = entry.title
        entry_author.text = entry.author
        entry_comments.text = getString(R.string.number_of_comments, entry.numComments)
        entry_published.text = entry.created.toString() // todo make it look XX ago
        entry_image.isGone = !entry.hasImage
        if (entry.hasImage) {
            entry_image.setImageURI(entry.thumbnail)
            entry_image.setOnClickListener {
                startActivity(
                    Intent(Intent.ACTION_VIEW).apply { data = entry.thumbnail.toUri() }
                )
            }
        }
    }
}