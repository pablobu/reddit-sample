package xyz.pablobu.redditsample

import android.app.Application
import com.facebook.drawee.backends.pipeline.Fresco
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import xyz.pablobu.redditsample.di.networkModule
import xyz.pablobu.redditsample.di.repositoryModule
import xyz.pablobu.redditsample.di.viewModelModule

class RedditSampleApp : Application() {

    override fun onCreate() {
        super.onCreate()
        Fresco.initialize(this)
        startKoin {
            // use AndroidLogger as Koin Logger
            androidLogger()

            // use the Android context given there
            androidContext(this@RedditSampleApp)

            // module list
            modules(networkModule, repositoryModule, viewModelModule)
        }
    }
}