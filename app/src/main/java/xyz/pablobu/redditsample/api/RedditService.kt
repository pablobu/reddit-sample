package xyz.pablobu.redditsample.api

import retrofit2.Call
import retrofit2.http.GET

interface RedditService {

    @GET("top.json")
    fun getTopList(): Call<TopResponse>
}