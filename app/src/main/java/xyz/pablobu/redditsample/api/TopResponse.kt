package xyz.pablobu.redditsample.api

/** Data model from top.json */
data class TopResponse(
    val data: TopData,
    val kind: String
)

data class TopData(
    val after: String,
    val before: String?,
    val children: List<Children>,
    val dist: Int
)

data class Children(
    val data: ChildrenData,
    val kind: String
)

data class ChildrenData(
    val id: String,
    val title: String,
    val author: String,
    val num_comments: Int,
    val thumbnail: String,
    val created: Long
)

