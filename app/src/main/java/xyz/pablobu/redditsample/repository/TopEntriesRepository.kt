package xyz.pablobu.redditsample.repository

import android.content.SharedPreferences
import androidx.core.content.edit
import xyz.pablobu.redditsample.api.Children
import xyz.pablobu.redditsample.api.RedditService
import xyz.pablobu.redditsample.model.RedditEntry

interface TopEntriesRepository {

    /** Get latest top entries */
    fun refreshTopEntries(): List<RedditEntry>

    /** Get latest top entries available in memory */
    fun getTopEntries(): List<RedditEntry>

    /** Mark an entry as read */
    fun markEntryAsRead(entry: RedditEntry)
}

class TopEntriesRepositoryImpl(
    private val service: RedditService,
    private val sharedPreferences: SharedPreferences
) : TopEntriesRepository {

    companion object {
        private const val READ_ENTRIES = "read-entries"
    }

    private var entriesCache: List<RedditEntry> = emptyList()

    override fun refreshTopEntries(): List<RedditEntry> {
        val response = service.getTopList().execute()
        if (response.isSuccessful) {
            val readEntries = sharedPreferences.getStringSet(READ_ENTRIES, emptySet()).orEmpty()
            entriesCache = response.body()?.data?.children
                ?.map {
                    it.getRedditEntry()
                }
                ?.map {
                    it.copy(unread = !readEntries.contains(it.id))
                }.orEmpty()
        }
        return entriesCache
    }

    override fun getTopEntries(): List<RedditEntry> {
        return entriesCache
    }

    override fun markEntryAsRead(entry: RedditEntry) {
        val readEntries = sharedPreferences.getStringSet(READ_ENTRIES, emptySet())
            .orEmpty()
            .toMutableSet()
            .apply { add(entry.id) }
        sharedPreferences.edit {
            putStringSet(
                READ_ENTRIES,
                readEntries
            ).commit()
        }
        entriesCache = entriesCache.map { it.copy(unread = !readEntries.contains(it.id)) }
    }
}

fun Children.getRedditEntry(): RedditEntry {
    return with(data) {
        RedditEntry(
            id = id,
            title = title,
            author = author,
            numComments = num_comments,
            thumbnail = thumbnail,
            created = created * 1000, // convert to millis
            unread = true
        )
    }
}