package xyz.pablobu.redditsample.di

import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import xyz.pablobu.redditsample.ui.top.TopEntriesViewModel

val viewModelModule = module {
    viewModel { TopEntriesViewModel(get()) }
}