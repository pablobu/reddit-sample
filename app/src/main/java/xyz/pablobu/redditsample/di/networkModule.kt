package xyz.pablobu.redditsample.di

import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import xyz.pablobu.redditsample.api.RedditService

val networkModule = module {
    single { get<Retrofit>().create(RedditService::class.java) }
    single {
        Retrofit.Builder()
            .baseUrl("https://www.reddit.com") // todo move to constant
            .addConverterFactory(MoshiConverterFactory.create())
            .build()
    }
}