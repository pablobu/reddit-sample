package xyz.pablobu.redditsample.di

import android.content.Context
import org.koin.dsl.module
import xyz.pablobu.redditsample.repository.TopEntriesRepository
import xyz.pablobu.redditsample.repository.TopEntriesRepositoryImpl

private const val PREFS_NAME = "RedditSample"

val repositoryModule = module {
    single { TopEntriesRepositoryImpl(get(), get()) as TopEntriesRepository  }
    single { get<Context>().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE) }
}