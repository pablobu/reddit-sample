package xyz.pablobu.redditsample.model

import android.os.Parcelable
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize

@Parcelize
data class RedditEntry(
    val id: String,
    val title: String,
    val author: String,
    val numComments: Int,
    val thumbnail: String,
    val created: Long,
    val unread: Boolean
) : Parcelable {
    @IgnoredOnParcel
    val hasImage = thumbnail != "self" && thumbnail != "default"
}